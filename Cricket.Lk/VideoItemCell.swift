//
//  VideoItemCell.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 10/22/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit
import FlatUIKit
class VideoItemCell: UITableViewCell {

    @IBOutlet var thumbNail: UIImageView!
    @IBOutlet var videoTitle: UILabel!
    @IBOutlet var primiumButton: FUIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
