//
//  SliderMenuModel.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 10/21/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit
import Foundation
class SliderMenuModel: NSObject {

    func countOfRowsInSliderMenu(section : Int) -> Int{
        if section == 0 {
           return SliderMenuItems.count + 1 // +1 is for headerCell
        }else{
            return 1 // header Cell Only
        }
        
    }
    
    func dataForSliderMenuItems (atRow : Int) -> [String : String] {
        var dataObject = [String:String]()
        
        if atRow == 1 {
            if let menuItems = SliderMenuItems(rawValue: atRow - 1) {
                dataObject = ["img":menuItems.sliderMenuItemNames() , "menu" : menuItems.sliderMenuItemNames(),"detail" : "detailImage"]
                return dataObject
            }else{
                return ["img" : "Default" , "menu" : "","detail" : "detailImage"]
            }
        }else{
            if let menuItems = SliderMenuItems(rawValue: atRow - 1) {
                dataObject = ["img":menuItems.sliderMenuItemNames() , "menu" : menuItems.sliderMenuItemNames(),"detail" : ""]
                return dataObject
            }else{
                return ["img" : "Default" , "menu" : "","detail" : ""]
            }
        }
        
    }
    
    func titleOfSliderMenu(atRow : Int) -> String{
        if let menuItems = SliderMenuItems(rawValue: atRow - 1) {
            return menuItems.sliderMenuItemNames()
        }else{
            return "Menu Item"
        }
    }
    
    func headerProfileName() -> (name : String , otherInfo : String) {
        return ("Gishantha Darshana" , "Team Sri Lanka")
    }
    
    func iconForItemName (atRow : Int) -> UIImage {
        if let menuItem = SliderMenuItems(rawValue: atRow - 1) {
            return menuItem.image()
        }else{
            return UIImage(named: "Default")!
        }
    }
}
