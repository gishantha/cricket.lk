//
//  MenuCell.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/18/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet var showDetail: UIImageView!
    @IBOutlet var menuName: UILabel!
    @IBOutlet var menuIcon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setMenuTitleAndImage (icon: String,name: String, andImage detail: String){
        menuIcon.image = UIImage(named: icon)
        if detail != "" {
            showDetail.image = UIImage(named: detail)
        }
        menuName.textColor = Colors.sliderMenuTextColor
        menuName.text = name
        
    }

}
