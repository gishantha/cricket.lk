//
//  ChannelCell.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/19/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet var videoThumbnail: UIImageView!
    @IBOutlet var videoTitle: UILabel!
    @IBOutlet var rating: FloatRatingView!
    @IBOutlet var views: UILabel!
    @IBOutlet var comments: UILabel!
    @IBOutlet var likes: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
