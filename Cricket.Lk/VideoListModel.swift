//
//  VideoListModel.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 10/22/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit

class VideoListModel: NSObject {

    func countOfRowsInVideoList(section : Int) -> Int{
        return VideoListItems.count
    }
    
    func titleOfVideoList(atRow : Int) -> String{
        if let menuItems = VideoListItems(rawValue: atRow) {
            return menuItems.videoItems()
        }else{
            return "Menu Item"
        }
    }
    
    func thumbnailForVideoItem (atRow : Int) -> UIImage {
        if let _ = VideoListItems(rawValue: atRow) {
            //return menuItem.thumbnail()
            return UIImage(named: "thumbnail")!
        }else{
            return UIImage(named: "thumbnail")!
        }
    }

}
