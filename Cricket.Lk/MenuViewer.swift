//
//  MenuViewer.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/18/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit

class MenuViewer: RootViewControllar {

    // MARK: - Outlets
    @IBOutlet var menuTableView: YUTableView!
    @IBOutlet var sliderMenuModel: SliderMenuModel!
    
    // MARK: - Variables
    var insertRowAnimation: UITableViewRowAnimation = .Top;
    var deleteRowAnimation: UITableViewRowAnimation = .Bottom;
    var allNodes : [YUTableViewNode]!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ModifyNavigationBar("Menu")
        self.view.backgroundColor = Colors.sliderMenuBackgroundColor
        setTableProperties ()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Expandable TableView Methods
    func setTableProperties () {
        allNodes = createNodes();
        menuTableView.setNodes(allNodes);
        menuTableView.setDelegate(self);
        menuTableView.insertRowAnimation = insertRowAnimation;
        menuTableView.deleteRowAnimation = deleteRowAnimation;
        menuTableView.animationCompetitionHandler = {
            print("Animation ended");
        }
    }
    
    // set expand and Collaps Animations
    // Currently no need of this method , because we are hard coded the Animation
    
    func setTableViewSettings (insertAnimation: UITableViewRowAnimation, deleteAnimation: UITableViewRowAnimation) {
        self.insertRowAnimation = insertAnimation;
        self.deleteRowAnimation = deleteAnimation;
    }
    
    
    // Creating menu items with child nodes
    
    func createNodes () -> [YUTableViewNode] {
        var nodes = [YUTableViewNode] ();
        for i in 0..<sliderMenuModel.countOfRowsInSliderMenu(0) {
            
            var childNodes = [YUTableViewNode] ()
            if i == 0 {
                let node = YUTableViewNode (childNodes: nil, data: ["img": "Profile", "name": "Gishantha Darshana" , "team" : "Sri Lanka"], cellIdentifier: "headBannarCell")
                nodes.append (node);
            }else{
                if i == 1 {
                    for j in 1...3 {
                        let node = YUTableViewNode (data: "\(i).\(j)", cellIdentifier: "BasicCell");
                        childNodes.append(node);
                    }
                }
                let node = YUTableViewNode(childNodes: childNodes, data: sliderMenuModel.dataForSliderMenuItems(i), cellIdentifier: "menuItemCell");
                nodes.append (node);
            }
            
            
        }
        return nodes;
    }

}

// MARK: - Expandable TableView Delegate

extension MenuViewer: YUTableViewDelegate {
    func setContentsOfCell(cell: UITableViewCell, node: YUTableViewNode) {
        if let customCell = cell as? HeaderCell, let cellDic = node.data as? [String:String] {
            
            customCell.setValues(cellDic["team"]!,profile: cellDic["name"]!, andImage: cellDic["img"]!);
        } else if let menu = cell as? MenuCell , let cellDic = node.data as? [String:String]{
            
            menu.setMenuTitleAndImage(cellDic["img"]!, name: cellDic["menu"]!, andImage: cellDic["detail"]!)
            
        }else{
            cell.textLabel!.text = node.data as? String;
        }
    }
    func heightForNode(node: YUTableViewNode) -> CGFloat? {
        if node.cellIdentifier == "headBannarCell" {
            return 162;
        }else{
            return 43;
        }
        
    }
    
    func didSelectNode(node: YUTableViewNode, indexPath: NSIndexPath) {
        print("Nodes : \(node.data)")
        if indexPath.row != 0 {
            if !node.hasChildren () {
                if node.data is String {
                    showViewAccordingToMenuItem("MainNavigation")
                }else {
                    if let menuName = node.data["menu"] as? String {
                        if menuName == "Settings" {
                            showViewAccordingToMenuItem("settingsNavigation")
                        }
                        if menuName == "SocialFeeds" {
                            showViewAccordingToMenuItem("SocialFeedNavigation")
                        }
                        if menuName == "Channels" {
                            showViewAccordingToMenuItem("channelsNavigation")
                        }
                    }
                }
            }
        }
    }
    
    private func showViewAccordingToMenuItem(viewIdentifier : String){
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            let mainNavigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(viewIdentifier) as! UINavigationController
            drawerController.mainViewController = mainNavigation
            drawerController.setDrawerState(.Closed, animated: true)
        }
    }
    
    
}

