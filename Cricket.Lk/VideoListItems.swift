//
//  VideoListItems.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 10/22/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import Foundation
import UIKit
enum VideoListItems : Int {
    case sangaBest
    case JayasooriyaSix
    case mahelaFifty
    case thirimannaManOfMatch
    case chamindaVasHatric
    case muralitharanWicket
    case sangaFairwell
    case worldcup
    
    
    
    static let videos =
    [
        sangaBest:"Sanga's Best Moments",
        JayasooriyaSix:"Jayasooriya's massive sixes",
        mahelaFifty:"Mahela Jayawardene's first 50",
        thirimannaManOfMatch:"Thirimanne is the man of the match",
        chamindaVasHatric:"Chaminda Vas Hatrick",
        muralitharanWicket:"Muralitharan's Wicket",
        sangaFairwell:"Sanga says Good Bye",
        worldcup:"2014 T20 Worldcup"
    ]
    
    func videoItems ()-> String {
        if let item = VideoListItems.videos[self] {
            return item
        }else {
            return "No Data"
        }
    }
    
    func thumbnail () -> UIImage {
        return UIImage(named: videoItems())!
    }
    
    static var count: Int { return VideoListItems.videos.count }
}