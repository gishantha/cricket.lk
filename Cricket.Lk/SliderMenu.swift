//
//  SliderMenu.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 10/20/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit

class SliderMenu: UITableViewController {

    @IBOutlet var menuTableView: YUTableView!
    
    @IBOutlet var sliderMenuModel: SliderMenuModel!
    
    
    var insertRowAnimation: UITableViewRowAnimation = .Top;
    var deleteRowAnimation: UITableViewRowAnimation = .Bottom;
    var allNodes : [YUTableViewNode]!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.sliderMenuBackgroundColor
        setTableProperties ()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTableProperties () {
        allNodes = createNodes();
        menuTableView.setNodes(allNodes);
        menuTableView.setDelegate(self);
        //menuTableView.allowOnlyOneActiveNodeInSameLevel = closeOtherNodes;
        menuTableView.insertRowAnimation = insertRowAnimation;
        menuTableView.deleteRowAnimation = deleteRowAnimation;
        menuTableView.animationCompetitionHandler = {
            print("Animation ended");
        }
    }
    
    func setTableViewSettings (insertAnimation: UITableViewRowAnimation, deleteAnimation: UITableViewRowAnimation) {
        //self.closeOtherNodes = closeOtherNodes;
        self.insertRowAnimation = insertAnimation;
        self.deleteRowAnimation = deleteAnimation;
    }
    
    func createNodes () -> [YUTableViewNode] {
        var nodes = [YUTableViewNode] ();
        for i in 1..<11 {
            var childNodes = [YUTableViewNode] ();
            for j in 1...5 {
                //var grandChildNodes = [YUTableViewNode] ();
                /*for k in 1...3 {
                let node = YUTableViewNode (data: "\(i).\(j)", cellIdentifier: "BasicCell");
                grandChildNodes.append(node);
                }*/
                //let node1 = YUTableViewNode(childNodes: grandChildNodes, data: ["img": "cat", "label": "\(i).\(j)"], cellIdentifier: "ComplexCell");
                let node = YUTableViewNode (data: "\(i).\(j)", cellIdentifier: "BasicCell");
                childNodes.append(node);
            }
            if i == 1 {
                let node = YUTableViewNode (childNodes: nil, data: ["img": "Profile", "name": "Gishantha Darshana" , "team" : "Sri Lanka"], cellIdentifier: "headBannarCell")
                nodes.append (node);
            }else{
                let node = YUTableViewNode(childNodes: childNodes, data: sliderMenuModel.titleOfSliderMenu(i), cellIdentifier: "menuItemCell");
                nodes.append (node);
            }
            
            
        }
        return nodes;
    }
    
  /*  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            let mainNavigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MainNavigation") as! UINavigationController
            let backgroundColor: UIColor
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            switch indexPath.row {
            case 1:
                backgroundColor = UIColor.redColor()
                mainNavigation.topViewController?.view.backgroundColor = backgroundColor
            case 2:
                backgroundColor = UIColor.blueColor()
                mainNavigation.topViewController?.view.backgroundColor = backgroundColor
            case 7 :
                let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("LoginScreen") as! Login
                self.presentViewController(nextViewController, animated:true, completion:nil)
            default:
                backgroundColor = UIColor.whiteColor()
                mainNavigation.topViewController?.view.backgroundColor = backgroundColor
            }
            
            drawerController.mainViewController = mainNavigation
            drawerController.setDrawerState(.Closed, animated: true)
        }
    }
*/
    
 
   /* override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sliderMenuModel.countOfRowsInSliderMenu(section)
    }

   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("headBannarCell", forIndexPath: indexPath) as! HeaderCell
            // Configure the cell...
            configureHeadBannerCell(cell)
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("menuItemCell", forIndexPath: indexPath)
            
            // Configure the cell...
            configureMenuItemCell(cell, atRow: indexPath.row)
            return cell
        }
        
        
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height : CGFloat?
        
        if indexPath.row == 0 {
            height = 162
        }else{
            height = 43
        }
        
        
        return height!
    }

    func configureHeadBannerCell (cell : HeaderCell){
        cell.profilePicture.image = UIImage(named: "Profile")
        cell.profileNeme.text = sliderMenuModel.headerProfileName().name
        cell.otherInfo.text = sliderMenuModel.headerProfileName().otherInfo
    }
    
    func configureMenuItemCell (cell : UITableViewCell,atRow : Int){
        
        cell.backgroundColor = Colors.sliderMenuItemBackgroundColor
        cell.imageView?.image = sliderMenuModel.iconForItemName(atRow)
        cell.textLabel?.textColor = Colors.sliderMenuTextColor
        cell.textLabel?.text = sliderMenuModel.titleOfSliderMenu(atRow)
    }
    
    
    
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}
extension SliderMenu: YUTableViewDelegate {
    func setContentsOfCell(cell: UITableViewCell, node: YUTableViewNode) {
        if let customCell = cell as? HeaderCell, let cellDic = node.data as? [String:String] {
            
            customCell.setValues(cellDic["team"]!,profile: cellDic["name"]!, andImage: cellDic["img"]!);
        } else {
            cell.textLabel!.text = node.data as? String;
        }
    }
    func heightForNode(node: YUTableViewNode) -> CGFloat? {
        if node.cellIdentifier == "headBannarCell" {
            return 162;
        }else{
            return 43;
        }
        
    }
    
    func didSelectNode(node: YUTableViewNode, indexPath: NSIndexPath) {
        if !node.hasChildren () {
            let alert = UIAlertView(title: "Row Selected", message: "Label: \(node.data as! String)", delegate: nil, cancelButtonTitle: "OK");
            alert.show();
        }else{
            if let drawerController = navigationController?.parentViewController as? KYDrawerController {
                let mainNavigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MainNavigation") as! UINavigationController
                let backgroundColor: UIColor
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                switch indexPath.row {
                case 1:
                    backgroundColor = UIColor.redColor()
                    mainNavigation.topViewController?.view.backgroundColor = backgroundColor
                case 2:
                    backgroundColor = UIColor.blueColor()
                    mainNavigation.topViewController?.view.backgroundColor = backgroundColor
                case 7 :
                    let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("LoginScreen") as! Login
                    self.presentViewController(nextViewController, animated:true, completion:nil)
                default:
                    backgroundColor = UIColor.whiteColor()
                    mainNavigation.topViewController?.view.backgroundColor = backgroundColor
                }
                
                drawerController.mainViewController = mainNavigation
                drawerController.setDrawerState(.Closed, animated: true)
            }
            
        }
    }
}




