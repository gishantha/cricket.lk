//
//  RootViewControllar.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/18/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit

class RootViewControllar: UIViewController {

    
    let defaultColor = Colors.NavigationBarColor
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ModifyNavigationBar (title : String){
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.title = title
        // Set Navigation bar background color
        self.navigationController?.navigationBar.barTintColor = defaultColor
        // Set Navigation bar tintColor (text color)
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        //self.navigationItem.titleView = titleLable;
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
