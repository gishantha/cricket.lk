//
//  SocialFeeds.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/19/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit
import FlatUIKit
class SocialFeeds: RootViewControllar , UITableViewDelegate , UITableViewDataSource{

    @IBOutlet var feedControllar: FUISegmentedControl!
    @IBOutlet var socialFeedTable: UITableView!
    @IBOutlet var mySocialFeed: SocialFeedModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        socialFeedTable.delegate = self
        socialFeedTable.dataSource = self
        
        //NavigationBar
        ModifyNavigationBar("Social Feeds")
        
        //configureFeedControllar
        configureFeedControllar ()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView Delegate Methods
    func numberOfSectionsInTableView(tableView:UITableView)->Int
    {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mySocialFeed.countOfRowsInFeedList(section)
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SocialFeedCell", forIndexPath: indexPath)
        ConfigureCell(cell, indexpath: indexPath)
        return cell
    }
    
    // MARK: - Cell Configurations
    
    func ConfigureCell (cell : UITableViewCell , indexpath : NSIndexPath) {
        
        let seperatorImage : UIImageView = UIImageView.init(image: UIImage(named: "seperator"))
        seperatorImage.frame = CGRectMake(0, cell.contentView.frame.size.height - 0.5, cell.contentView.frame.size.width, 2)
        cell.contentView.addSubview(seperatorImage)
        cell.textLabel?.textColor = Colors.sliderMenuTextColor
        cell.textLabel?.text = mySocialFeed.titleOfFeedList(indexpath.row)
    }
    
    @IBAction func openSlidMenu(sender: AnyObject) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
    
    // FeedControllar Action
    @IBAction func onChangeFeedType(sender: AnyObject) {
        switch sender.selectedSegmentIndex {
        case 0 :
            sender.setImage(UIImage(named: "facebookSegment"), forSegmentAtIndex: 0)
        case 1:
           sender.setImage(UIImage(named: "twitterSegment"), forSegmentAtIndex: 1)
        case 2:
            sender.setImage(UIImage(named: "gPlusSegment"), forSegmentAtIndex: 2)
        default:
            print("Default")
        }

    }
    
    
     // MARK: - Configuration Methods
    
    func configureFeedControllar (){
        self.feedControllar.backgroundColor = Colors.sliderMenuBackgroundColor
        self.feedControllar.selectedColor = Colors.segmentedControllarSelectedColor
        self.feedControllar.deselectedColor = Colors.segmentedControllarUnSelectedColor
        self.feedControllar.dividerColor = Colors.segmentedControllarSeperatorColor
        self.feedControllar.cornerRadius = 10.0
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
