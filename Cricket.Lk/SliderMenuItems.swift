//
//  SliderMenuItems.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 10/21/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import Foundation
import UIKit
enum SliderMenuItems : Int {
    case LiveStreams
    case Videos
    case Channels
    case Scores_Statistics
    case SocialFeeds
    case Recommendations
    case Tournaments
    case Settings
    //case SignOut
    
    
    static let sliderMenuNames =
    [
        LiveStreams : "Live Streams" ,
        Videos : "Videos" ,
        Channels : "Channels" ,
        Scores_Statistics : "Scores and Statistics" ,
        SocialFeeds : "SocialFeeds" ,
        Recommendations : "Recommendations" ,
        Tournaments : "Tournaments",
        Settings : "Settings",
        //SignOut : "SignOut"
    ]
    
    func sliderMenuItemNames() -> String {
        if let menuItemName = SliderMenuItems.sliderMenuNames[self] {
            return menuItemName
        } else {
            return "Default"
        }
    }
    
    func image() -> UIImage {
        return UIImage(named: sliderMenuItemNames())!
    }
    
    static var count: Int { return SliderMenuItems.sliderMenuNames.count }
    
}
