//
//  Home.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 10/20/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit
import FlatUIKit
class Home: RootViewControllar , UITableViewDelegate , UITableViewDataSource {

    // MARK: - Properties
    @IBOutlet var videoCategeryControllar: FUISegmentedControl!
    @IBOutlet var videoModel: VideoListModel!
    @IBOutlet var videoList: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ModifyNavigationBar("Home")
        registerNibClass()
        self.videoList.delegate = self
        self.videoList.dataSource = self
        
        setupUIControllars()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - SliderMenu Button
    @IBAction func OpenLeftDrawer(sender: AnyObject) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
    // MARK: - Segmented Controllar Action
    @IBAction func loadVideoCategories(sender: AnyObject) {
        switch sender.selectedSegmentIndex {
        case 0 :
            print("")
        case 1:
            print("")
        case 2:
            print("")
        default:
            print("Default")
        }
    }
    
    // MARK: - TableView Delegate Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videoModel.countOfRowsInVideoList(section)
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 91.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("VideoItemCell", forIndexPath: indexPath) as! VideoItemCell
        
        // Configure the cell...
        configureVideoListCell(cell, atRow: indexPath.row)
        return cell
    }
    
    func configureVideoListCell (cell : VideoItemCell,atRow : Int) {
        //cell.backgroundColor = Colors.videoListItemColor
        cell.videoTitle.textColor = Colors.videoListTextColor
        cell.videoTitle.text = videoModel.titleOfVideoList(atRow)
        cell.thumbNail.image = videoModel.thumbnailForVideoItem(atRow)
    }
    
    // MARK: - Helper Methods
    func registerNibClass(){
        videoList.registerNib(UINib(nibName: "VideoItemCell", bundle: nil), forCellReuseIdentifier: "VideoItemCell")
    }
    
    func setupUIControllars(){
        func setUpSegmentedControllar(){
            self.videoCategeryControllar.selectedFontColor = Colors.defaultFontColor
            self.videoCategeryControllar.selectedColor = Colors.segmentedControllarSelectedColor
            self.videoCategeryControllar.deselectedColor = Colors.segmentedControllarUnSelectedColor
            self.videoCategeryControllar.dividerColor = Colors.segmentedControllarSeperatorColor
            self.videoCategeryControllar.cornerRadius = 2.0
        }
        
        
        setUpSegmentedControllar()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
