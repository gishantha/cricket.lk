//
//  SocialFeedModel.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/19/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit

class SocialFeedModel: NSObject {

    func countOfRowsInFeedList(section : Int) -> Int{
        return SocialFeedItems.count
    }
    
    func titleOfFeedList(atRow : Int) -> String{
        if let menuItems = SocialFeedItems(rawValue: atRow) {
            return menuItems.feedItems()
        }else{
            return "No Feed"
        }
    }
    
}
