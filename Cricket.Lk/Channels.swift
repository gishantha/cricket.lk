//
//  Channels.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/19/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit

class Channels: RootViewControllar , UITableViewDataSource , UITableViewDelegate{

    @IBOutlet var channelTableView: UITableView!
    @IBOutlet var channelModel: VideoListModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        ModifyNavigationBar("Channels")
        channelTableView.delegate = self
        channelTableView.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        
        
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - TableView Delegate Methods
    
    func numberOfSectionsInTableView(tableView:UITableView)->Int
    {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channelModel.countOfRowsInVideoList(section)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 94.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ChannelCell", forIndexPath: indexPath) as! ChannelCell
        configureCell(cell, indexpath: indexPath)
        return cell
    }
    
    
    // MARK: - Cell Configurations
    
    func configureCell (cell : ChannelCell , indexpath : NSIndexPath) {
        
        let seperatorImage : UIImageView = UIImageView.init(image: UIImage(named: "seperator"))
        seperatorImage.frame = CGRectMake(0, cell.contentView.frame.size.height - 0.5, cell.contentView.frame.size.width, 2)
        cell.contentView.addSubview(seperatorImage)
        cell.videoTitle.textColor = Colors.sliderMenuTextColor
        cell.videoTitle.text = channelModel.titleOfVideoList(indexpath.row)
        cell.videoThumbnail.image = channelModel.thumbnailForVideoItem(indexpath.row)
    }
    
    @IBAction func openSlidMenu(sender: AnyObject) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
