//
//  SocialFeedItems.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/19/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import Foundation
import UIKit
enum SocialFeedItems : Int {
    case worldCup
    case twenyTwenty
    case jayasooriya
    case sanga
    case mahela
    case fans
    
    static let feeds = [
        worldCup : "#worldCup , #final , #Heros , #champianos , #Lions" ,
        twenyTwenty : "#T20 , #OurGame , #sangaMaiya , #SriLanka , #Final" ,
        jayasooriya : "#pasCommingOut , #aiyoooo , #matapissuOooi , #winaasai" ,
        sanga : "#MyHero , #Legend , #SriLankan , #LionRoar" ,
        mahela : "#MaiyaDeiya ,#maiya50 , #BestCaptain , #LifeSaver" ,
        fans : "#buduAmmo , #patta ,#premadasa , #BigFun"
    ]
    
    func feedItems ()-> String {
        if let item = SocialFeedItems.feeds[self] {
            return item
        }else {
            return "No Data"
        }
    }
    
    func thumbnail () -> UIImage {
        return UIImage(named: feedItems())!
    }
    
    static var count: Int { return SocialFeedItems.feeds.count }
}