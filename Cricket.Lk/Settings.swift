//
//  Settings.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/19/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit
import FlatUIKit
class Settings: RootViewControllar {

    // MARK: - Outlets
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var profileName: UILabel!
    @IBOutlet var profileRegistrationType: UILabel!
    @IBOutlet var logOutButton: FUIButton!
    @IBOutlet var subscriptionType: UILabel!
    @IBOutlet var subscriptionRegistrationType: UILabel!
    @IBOutlet var upgradeSubscriptionButton: FUIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ModifyNavigationBar("Settings")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func openSliderMenu(sender: AnyObject) {
        if let drawerController = navigationController?.parentViewController as? KYDrawerController {
            drawerController.setDrawerState(.Opened, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
