//
//  Login.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 11/9/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import UIKit
import TwitterKit
import FlatUIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Google
class Login: UIViewController , GIDSignInUIDelegate , GIDSignInDelegate{

    @IBOutlet var googleSignInButton: FUIButton!
    @IBOutlet var googleLogIn: GIDSignInButton!
    @IBOutlet var signUpButton: FUIButton!
    @IBOutlet var twitterButton: UIButton!
    @IBOutlet var fbButton: FUIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        //GIDSignIn.sharedInstance().delegate = self
        //GIDSignIn.sharedInstance().uiDelegate = self
        //GIDSignIn.sharedInstance().uiDelegate = self
        
        //googleLogIn.style = GIDSignInButtonStyle.Standard
        //googleLogIn.colorScheme = GIDSignInButtonColorScheme.Dark
        
        //googleLogIn.clipsToBounds = true
        //googleLogIn.sizeToFit()
        
        
        googleSignInButton.addTarget(self, action: "googleCustomLogin", forControlEvents: UIControlEvents.TouchUpInside)
        
        //googleLogIn.delegate = self
        
        // Uncomment to automatically sign in the user.
       // GIDSignIn.sharedInstance().signInSilently()
        
        // TODO(developer) Configure the sign-in button look/feel
        // ...
    }

    
    func googleCustomLogin ()-> Void {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()

    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        /*if (PFUser.currentUser() == nil) {
            let loginViewController = PFLogInViewController()
            loginViewController.delegate = self
            self.presentViewController(loginViewController, animated: false, completion: nil)
        }*/
        setKeyBoardNotificationListner()
        setTapListner ()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func twitterLogin(sender: AnyObject) {
        Twitter.sharedInstance().logInWithCompletion { session, error in
            if (session != nil) {
                print("signed in as \(session!.userName)");
            } else {
                print("error: \(error!.localizedDescription)");
            }
        }
    }
    @IBAction func signUpAction(sender: AnyObject) {
        performSegueWithIdentifier("loged", sender: self)
    }
    
    
    
    
    @IBAction func facebookLogin(sender: AnyObject) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager .logInWithReadPermissions(["email"], fromViewController: self, handler: { (result, error) -> Void in
            if (error == nil && result != nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result
                if( fbloginresult.grantedPermissions != nil && fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                    fbLoginManager.logOut()
                }
            }
        })
        
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.currentAccessToken()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    print(result)
                    self.performSegueWithIdentifier("loged", sender: self)
                }
            })
        }
    }

    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
        presentViewController viewController: UIViewController!) {
            self.presentViewController(viewController, animated: true, completion: nil)
    }
  
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
        dismissViewController viewController: UIViewController!) {
            self.dismissViewControllerAnimated(true, completion: nil)
    }
   
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
        withError error: NSError!) {
            if (error == nil) {
                // Perform any operations on signed in user here.
                let userId = user.userID                  // For client-side use only!
                let idToken = user.authentication.idToken // Safe to send to the server
                let name = user.profile.name
                let email = user.profile.email
                print("\(userId)")
                print("\(idToken)")
                print("\(name)")
                print("\(email)")
                // ...
            } else {
                print("\(error.localizedDescription)")
            }
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
        withError error: NSError!) {
            // Perform any operations when the user disconnects from app here.
            // ...
    }
    
    
    @IBAction func ditTapSignOut(sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
    }
    
    
    // MARK: - Utility Methods
    
    func setKeyBoardNotificationListner(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }
    
    func hideKeyboardWhenTap (){
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= 150
        }
        /* var info = sender.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
        self.textBoxConstraint.constant = keyboardFrame.size.height - 20
        })
        */
    }
    func keyboardWillHide(sender: NSNotification) {
        
        if self.view.frame.origin.y == -150 {
            self.view.frame.origin.y += 150
        }
        
    }
    
    func setTapListner (){
        let tapRecognizer = UITapGestureRecognizer(target: self, action: "hideKeyboardWhenTap")
        tapRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapRecognizer)
    }

  /*
    func logInViewController(logInController: PFLogInViewController, didLogInUser user: PFUser) {
        self.dismissViewControllerAnimated(true, completion: nil)
        presentLoggedInAlert()
    }
    
    func signUpViewController(signUpController: PFSignUpViewController, didSignUpUser user: PFUser) {
        self.dismissViewControllerAnimated(true, completion: nil)
        presentLoggedInAlert()
    }
    
    func presentLoggedInAlert() {
        let alertController = UIAlertController(title: "You're logged in", message: "Welcome to Vay.K", preferredStyle: .Alert)
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
*/
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
