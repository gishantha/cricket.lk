//
//  Colors.swift
//  Cricket.Lk
//
//  Created by Gishantha Darshana on 10/21/15.
//  Copyright © 2015 Gishantha Darshana. All rights reserved.
//

import Foundation
import UIKit
struct Colors {
    static let sliderMenuItemBackgroundColor = UIColor(hexString: "#363636")
    static let sliderMenuBackgroundColor = UIColor(hexString: "#363636")
    static let sliderMenuTextColor = UIColor(hexString: "#FFFFFF")
    static let videoListItemColor = UIColor(hexString: "#248E5D")
    static let videoListBackgroundColor = UIColor(hexString: "#248E5D")
    static let videoListTextColor = UIColor(hexString: "#FFFFFF")
    static let segmentedControllarSelectedColor = UIColor(hexString: "#FFFFFF")
    static let segmentedControllarSeperatorColor = UIColor(hexString: "#FFFFFF")
    static let segmentedControllarUnSelectedColor = UIColor(hexString: "#014CA1")
    static let defaultFontColor = UIColor(hexString: "#4B4849")
    static let NavigationBarColor = UIColor(hexString: "#004DA5")
    
}
